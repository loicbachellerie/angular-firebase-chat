import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import * as firebase from 'firebase/app';

import { ChatMessage } from '../models/chat-message.model';

@Injectable()
export class ChatService {
  user: firebase.User;
  chatMessages: AngularFireList<ChatMessage[]>;
  chatMessage: ChatMessage;
  userName: Observable<string>;

  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth
    ) {
       this.afAuth.authState.subscribe(auth => {
         if (auth !== undefined && auth !== null) {
            this.user = auth;
          }

          this.getUser().subscribe(a => {
            this.userName = this.upperCaseFirst(a['displayName']);
            console.log(this.userName);
          });
        });
    }

  upperCaseFirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
  }

  getUser() {
    const userId = this.user.uid;
    const path = `/users/${userId}`;
    return this.db.object(path).valueChanges();
  }

  getUsers() {
    const path = '/users';
    return this.db.list(path);
  }

  sendMessage(msg: string) {
    const timestamp = this.getTimeStamp();
    const email =  this.user.email;
    this.chatMessages = this.getMessages();
    const ref = this.db.list('messages').query.ref.push();
    ref.set({message: msg,
      timeSent: timestamp,
      userName: this.userName, // this.userName,
      email: email});
  }

  getMessages(): AngularFireList<ChatMessage[]> {
    return this.db.list('/messages', ref => ref.orderByKey().limitToLast(25));
  }

  getTimeStamp() {
    const now = new Date();
    const date = now.getUTCFullYear() + '/' +
                 (now.getUTCMonth() + 1) + '/' +
                 now.getUTCDate();
    const time = now.getUTCHours() + ':' +
                 now.getUTCMinutes() + ':' +
                 now.getUTCSeconds();

    return (date + ' ' + time);
  }
}


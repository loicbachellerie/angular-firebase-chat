// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyA7iwMwPnKZGgizBLmsOo3yAosB-B3w1fs',
    authDomain: 'spark-d2a64.firebaseapp.com',
    databaseURL: 'https://spark-d2a64.firebaseio.com',
    projectId: 'spark-d2a64',
    storageBucket: 'spark-d2a64.appspot.com',
    messagingSenderId: '136634950180'
  }
};
